package bolao;

import bolao.model.Bet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

public class BetItemProcessor implements ItemProcessor<Bet, Bet> {

    private static final Logger log = LoggerFactory.getLogger(BetItemProcessor.class);

    @Override
    public Bet process(final Bet bet) throws Exception {
        
        log.info("Processing bet of user " + bet.getUserId());
        return bet;
    }
}
