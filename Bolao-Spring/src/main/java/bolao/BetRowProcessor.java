package bolao;

import bolao.model.Bet;
import bolao.model.Game;
import bolao.model.User;
import org.springframework.batch.item.ItemProcessor;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;


public class BetRowProcessor implements ItemProcessor<Bet, User> {
    @Override
    public User process(Bet bet) throws Exception {

        Connection conn = null;
        Statement stmt = null;

        int gameWinnerId = 0;
        int gameLooserId = 0;
        int gameWinnerScore = 0;
        int gameLooserScore = 0;
        int gameDiferenceOfGoals = 0;
        boolean gameDraw = false;

        int betWinnerId = 0;
        int betLooserId = 0;
        int betWinnerScore = 0;
        int betLooserScore = 0;
        int userDiferenceOfGoals = 0;
        boolean userDraw = false;

        userDiferenceOfGoals = bet.getScore1() - bet.getScore2();
        userDiferenceOfGoals = userDiferenceOfGoals < 0 ? userDiferenceOfGoals * -1 : userDiferenceOfGoals;


        conn = DriverManager.getConnection("jdbc:h2:mem:AZ;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE", "sa", "");

        stmt = conn.createStatement();

        String gameSql = "SELECT gameId, team1, score1, team2, score2 FROM Game where gameId = " + bet.getGameId();
        ResultSet gameRs = stmt.executeQuery(gameSql);
        
        Game game = new Game();

        if (gameRs.next()){
        	game.setTeam1Id(gameRs.getInt("team1"));
        	game.setScore1(gameRs.getInt("score1"));
        	game.setTeam2Id(gameRs.getInt("team2"));
        	game.setScore2(gameRs.getInt("score2"));
            
            // set game info
            if (game.getScore1() > game.getScore2()){
                gameWinnerId = game.getTeam1Id();
                gameLooserId = game.getTeam2Id();
                gameWinnerScore = game.getScore1();
                gameLooserScore = game.getScore2();
            } else if (game.getScore1() < game.getScore2())  {
            	gameWinnerId = game.getTeam2Id();
                gameLooserId = game.getTeam1Id();
                gameWinnerScore = game.getScore2();
                gameLooserScore = game.getScore1();
            } else{
                gameDraw = true;
            }

        }

        // set bet info
        if (bet.getScore1() > bet.getScore2()){
            betWinnerId = game.getTeam1Id();
            betLooserId = game.getTeam2Id();
            betWinnerScore = bet.getScore1();
            betLooserScore = bet.getScore2();
        } else if (bet.getScore1() < bet.getScore2()){
            betWinnerId = game.getTeam2Id();
            betLooserId = game.getTeam1Id();
            betWinnerScore = bet.getScore2();
            betLooserScore = bet.getScore1();
        } else{
            userDraw = true;
        }
        
        gameDiferenceOfGoals = gameWinnerScore - gameLooserScore;
        gameDiferenceOfGoals = gameDiferenceOfGoals < 0 ? gameDiferenceOfGoals * -1 : gameDiferenceOfGoals;


        String userSql = "SELECT name, points FROM user where userId = " + bet.getUserId();
        ResultSet userRs = stmt.executeQuery(userSql);


        if (userRs.next()){
            User user = new User(bet.getUserId(), userRs.getString("name"), userRs.getInt("points"));

            if (bet.getScore1() == game.getScore1() && bet.getScore2() == game.getScore2()){
            	user.setPoints(user.getPoints() + 20);
            } else if(betWinnerId == gameWinnerId && userDiferenceOfGoals == gameDiferenceOfGoals)  {
            	user.setPoints(user.getPoints() + 16);
            } else if (userDraw == true && gameDraw == true){
            	user.setPoints(user.getPoints() + 16);
            } else if (betWinnerId == gameWinnerId && (betWinnerScore == gameWinnerScore || betLooserScore == gameLooserScore)){
            	user.setPoints(user.getPoints() + 15);
            } else if (betWinnerId == gameWinnerId){
            	user.setPoints(user.getPoints() + 10);
            } else if (bet.getScore1() == game.getScore1() || bet.getScore2() == game.getScore2()){
            	user.setPoints(user.getPoints() + 5);
            }
        	return user;
 
        }
        return null;
    }

}
