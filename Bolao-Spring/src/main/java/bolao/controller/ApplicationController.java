
package bolao.controller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import bolao.model.User;

@RestController
public class ApplicationController {

	@GetMapping("/api")
	@CrossOrigin(origins = "http://localhost:4200")
	public Collection<User> getUsers() throws SQLException{
		
		Connection conn = null;
        Statement stmt = null;
        
        Collection<User> list = new ArrayList<>();
		
		conn = DriverManager.getConnection("jdbc:h2:mem:AZ;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE", "sa", "");

        stmt = conn.createStatement();
        
        String usersSql = "SELECT userId, name, points FROM User order by points desc";
        ResultSet usersRs = stmt.executeQuery(usersSql);
        
        while (usersRs.next()){
        	User user = new User (usersRs.getInt("userId"), usersRs.getString("name"), usersRs.getInt("points"));
        	list.add(user);
        }
		return list;
	}
}
