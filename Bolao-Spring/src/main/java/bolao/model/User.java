package bolao.model;

public class User {

	private int userId;
	private String name;
	private int points;
	
	public User(int userId, String name, int points) {
		this.userId = userId;
		this.name = name;
		this.points = points;
	}
	
	public void setUserId(int userId){
		this.userId = userId;
	}

	public int getUserId() {
		return userId;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public int getPoints() {
		return points;
	}

	public void setPoints(int points) {
		this.points = points;
	}

}
