package bolao.model;

public class Team {
	
	private int teamId;
	private String name;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getTeamId() {
		return teamId;
	}

}
