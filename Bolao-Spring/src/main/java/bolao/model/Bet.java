package bolao.model;

public class Bet {

    private int userId;
    private int gameId;
    private int score1;
    private int score2;

    public Bet() {
    }

    public Bet(int userId, int gameId, int score1, int score2) {
        this.userId = userId;
        this.gameId = gameId;
        this.score1 = score1;
        this.score2 = score2;
    }
    

    public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getGameId() {
		return gameId;
	}

	public void setGameId(int gameId) {
		this.gameId = gameId;
	}

	public int getScore1() {
		return score1;
	}

	public void setScore1(int score1) {
		this.score1 = score1;
	}

	public int getScore2() {
		return score2;
	}

	public void setScore2(int score2) {
		this.score2 = score2;
	}

	@Override
    public String toString() {
        return "user id: " + userId + ", game id: " + gameId + ", score 1: " + score1 + ", score 2: " + score2;
    }
}
