package bolao;


import javax.sql.DataSource;
import bolao.model.Bet;
import bolao.model.User;
import org.springframework.batch.core.*;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.database.*;
import org.springframework.batch.item.database.builder.JdbcBatchItemWriterBuilder;
import org.springframework.batch.item.database.support.H2PagingQueryProvider;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Configuration
@EnableBatchProcessing
@EnableScheduling
public class BatchConfiguration {

    @Autowired
    private SimpleJobLauncher jobLauncher;

    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    @Autowired
    public DataSource dataSource;

    @Autowired
    Job job;
    
    // every day at 12:00am
    @Scheduled(cron = "0 0 0 * * *")
    public void perform() throws Exception {

        System.out.println("Job Started at :" + new Date());

        JobParameters param = new JobParametersBuilder().addString("importJob",
                String.valueOf(System.currentTimeMillis())).toJobParameters();

        JobExecution execution = jobLauncher.run(job, new JobParameters());

        System.out.println("Job finished with status :" + execution.getStatus());
    }

    @Bean
    public JdbcCursorItemReader<Bet> reader(){
        JdbcCursorItemReader<Bet> reader = new JdbcCursorItemReader<Bet>();
        reader.setDataSource(dataSource);
        reader.setSql("SELECT userId, gameId, score1, score2 FROM Bet");
        reader.setRowMapper(new BetRowMapper());

        return reader;
    }

    public class BetRowMapper implements RowMapper<Bet> {

        @Override
        public Bet mapRow(ResultSet rs, int rowNum) throws SQLException {
            Bet bet = new Bet();
            bet.setUserId(rs.getInt("userId"));
            bet.setGameId(rs.getInt("gameId"));
            bet.setScore1(rs.getInt("socre1"));
            bet.setScore2(rs.getInt("score2"));

            return bet;
        }

    }

    @Bean
    public BetRowProcessor rowProcessor(){
        return new BetRowProcessor();
    }
    
    @Bean
    public BetItemProcessor betProcessor() {
        return new BetItemProcessor();
    }
    
    @Bean
    public FlatFileItemReader<Bet> csvReader() {
        return new FlatFileItemReaderBuilder<Bet>()
                .name("betItemReader")
                .resource(new ClassPathResource("sample-data.csv"))
                .delimited()
                .names(new String[]{"userId", "gameId", "score1", "score2"})
                .fieldSetMapper(new BeanWrapperFieldSetMapper<Bet>() {{
                    setTargetType(Bet.class);
                }})
                .build();
    }

    @Bean
    public ItemReader<Bet> databaseBetReader(DataSource dataSource) {
        JdbcPagingItemReader<Bet> databaseReader = new JdbcPagingItemReader<>();

        databaseReader.setDataSource(dataSource);
        databaseReader.setPageSize(1);

        PagingQueryProvider queryProvider = createQueryProvider();
        databaseReader.setQueryProvider(queryProvider);

        databaseReader.setRowMapper(new BeanPropertyRowMapper<>(Bet.class));

        return databaseReader;
    }

    private PagingQueryProvider createQueryProvider() {
        H2PagingQueryProvider queryProvider = new H2PagingQueryProvider();

        Map<String, Order> sortConfiguration = new HashMap<>();
        sortConfiguration.put("userId", Order.ASCENDING);

        queryProvider.setSelectClause("SELECT userId, gameId, score1, score2");
        queryProvider.setFromClause("FROM Bet");
        queryProvider.setSortKeys(sortConfiguration);

        return queryProvider;
    }
    
    @Bean
    public JdbcBatchItemWriter<Bet> betWriter(DataSource dataSource) {
        return new JdbcBatchItemWriterBuilder<Bet>()
                .itemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<>())
                .sql("INSERT INTO bet (userId, gameId, score1, score2) VALUES (:userId, :gameId, :score1, :score2)")
                .dataSource(dataSource)
                .build();
    }

    @Bean
    public JdbcBatchItemWriter<User> resultWriter(DataSource dataSource) {
        return new JdbcBatchItemWriterBuilder<User>()
                .itemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<>())
                .sql("UPDATE user SET points = :points where userId = :userId")
                .dataSource(dataSource)
                .build();
    }

    @Bean
    public Job importJob(JobCompletionNotificationListener listener, Step step1, Step step2) {
        return jobBuilderFactory.get("importJob")
                .incrementer(new RunIdIncrementer())
                .listener(listener)
                .flow(step1)
                .next(step2)
                .end()
                .build();
    }

    @Bean
    public Step step1(JdbcBatchItemWriter<Bet> writer) {
        return stepBuilderFactory.get("step1")
                .<Bet, Bet> chunk(10)
                .reader(csvReader())
                .processor(betProcessor())
                .writer(writer)
                .allowStartIfComplete(true)
                .build();
    }
    @Bean
    public Step step2(JdbcBatchItemWriter<User> writer, DataSource dataSource) {
        return stepBuilderFactory.get("step2")
                .<Bet, User> chunk(10)
                .reader(databaseBetReader(dataSource))
                .processor(rowProcessor())
                .writer(writer)
                .allowStartIfComplete(true)
                .build();
    }

}
