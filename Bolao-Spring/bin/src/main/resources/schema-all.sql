-- table user
DROP TABLE user IF EXISTS;
CREATE TABLE user  (
    userId BIGINT IDENTITY NOT NULL PRIMARY KEY,
    name VARCHAR(20),
    points int
);
-- populate user 
INSERT INTO user (name, points) VALUES ('Joao', 0);
INSERT INTO user (name, points) VALUES ('Marcus', 0);
INSERT INTO user (name, points) VALUES ('Ana', 0);
INSERT INTO user (name, points) VALUES ('Bia', 0);
INSERT INTO user (name, points) VALUES ('Carlos', 0);
INSERT INTO user (name, points) VALUES ('Alexandre', 0);
INSERT INTO user (name, points) VALUES ('Fabiana', 0);
INSERT INTO user (name, points) VALUES ('Lucas', 0);
INSERT INTO user (name, points) VALUES ('Mario', 0);
INSERT INTO user (name, points) VALUES ('Janaina', 0);


-- table team
DROP TABLE team IF EXISTS;
CREATE TABLE team  (
    teamId BIGINT IDENTITY NOT NULL PRIMARY KEY,
    name VARCHAR(20)
);
-- populate team
INSERT INTO team (name) VALUES ('Brasil');
INSERT INTO team (name) VALUES ('Argentina');
INSERT INTO team (name) VALUES ('Alemanha');
INSERT INTO team (name) VALUES ('Espanha');
INSERT INTO team (name) VALUES ('Japao');
INSERT INTO team (name) VALUES ('Australia');


-- table game
DROP TABLE game IF EXISTS;
CREATE TABLE game  (
    gameId BIGINT IDENTITY NOT NULL PRIMARY KEY,
    team1 int,
    score1 int,
    team2 int,
    score2 int,
    
    FOREIGN KEY (team1) REFERENCES team(teamId),
    FOREIGN KEY (team2) REFERENCES team(teamId)
);
-- populate game
INSERT INTO game (team1, score1, team2, score2) VALUES (1,3,2,0);
INSERT INTO game (team1, score1, team2, score2) VALUES (3,1,4,1);
INSERT INTO game (team1, score1, team2, score2) VALUES (5,1,6,0);
INSERT INTO game (team1, score1, team2, score2) VALUES (1,3,6,2);
INSERT INTO game (team1, score1, team2, score2) VALUES (2,0,5,2);
INSERT INTO game (team1, score1, team2, score2) VALUES (3,3,4,3);


-- table bet
DROP TABLE bet IF EXISTS;
CREATE TABLE bet  (
    betId BIGINT IDENTITY NOT NULL PRIMARY KEY,
	userid int,
    gameid int,
    score1 int,
    score2 int,
    
    FOREIGN KEY (userId) REFERENCES user(userId),
    FOREIGN KEY (gameId) REFERENCES game(gameId)
);

-- table result
DROP TABLE result IF EXISTS;
CREATE TABLE result  (
    userId int,
    points int,
    
    FOREIGN KEY (userId) REFERENCES user(userId)
);

INSERT INTO result (userId, points) VALUES (1,0);
INSERT INTO result (userId, points) VALUES (2,0);
INSERT INTO result (userId, points) VALUES (3,0);
INSERT INTO result (userId, points) VALUES (4,0);
INSERT INTO result (userId, points) VALUES (5,0);
INSERT INTO result (userId, points) VALUES (6,0);
INSERT INTO result (userId, points) VALUES (7,0);
INSERT INTO result (userId, points) VALUES (8,0);
INSERT INTO result (userId, points) VALUES (9,0);
INSERT INTO result (userId, points) VALUES (10,0);

